grammar plan;

plan_
    : component
    ;

component
    : PARENTESE_INICIAL 'component' id test+ notifier_global* PARENTESE_FINAL
    ;

test
    : PARENTESE_INICIAL 'test' expressao PARENTESE_FINAL
    ;

notifier_global
    : PARENTESE_INICIAL 'notifier' predicate driver PARENTESE_FINAL
    ;

notifier
    : PARENTESE_INICIAL 'notifier' predicate driver PARENTESE_FINAL
    ;

expressao
    : notifier* driver notifier*
    ;

driver
    : PARENTESE_INICIAL 'driver' id data+ PARENTESE_FINAL
    ;

predicate
    : COLCHETE_INICIAL value COLCHETE_FINAL
    ;

value
    : 'success'
    | 'failed'
    ;

id
    : ID_FRAGMENT
    ;

data
    : attribute string
    | attribute file
    ;

file
    : PARENTESE_INICIAL 'file'  string  PARENTESE_FINAL
    ;

attribute
    : ':' ID_FRAGMENT
    ;

string
    :  STRING
    ;

STRING
   : '"' DOUBLE_QUOTE_CHAR* '"'
   | '\'' SINGLE_QUOTE_CHAR* '\''
   ;

fragment DOUBLE_QUOTE_CHAR
   : ~["\\\r\n]
   | ESCAPE_SEQUENCE
   ;
fragment SINGLE_QUOTE_CHAR
   : ~['\\\r\n]
   | ESCAPE_SEQUENCE
   ;

fragment ESCAPE_SEQUENCE
   : '\\'
   ( NEWLINE
   | UNICODE_SEQUENCE       // \u1234
   | ['"\\/bfnrtv]          // single escape char
   | ~['"\\bfnrtv0-9xu\r\n] // non escape char
   | '0'                    // \0
   | 'x' HEX HEX            // \x3a
   )
   ;

fragment UNICODE_SEQUENCE
   : 'u' HEX HEX HEX HEX
   ;

fragment HEX
   : [0-9a-fA-F]
   ;

fragment NEWLINE
   : '\r\n'
   | [\r\n\u2028\u2029]
   ;

ID_FRAGMENT
    : [a-zA-Z]+
    ;

COLCHETE_INICIAL
    : '['
    ;

COLCHETE_FINAL
    : ']'
    ;

PARENTESE_INICIAL
    : '('
    ;

PARENTESE_FINAL
    : ')'
    ;

WS
   : [ \r\n\t]+ -> skip
   ;
