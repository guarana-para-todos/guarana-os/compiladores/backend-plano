package guarana.compilador.backend;

import guarana.compilador.backend.antlr.planBaseListener;
import guarana.compilador.backend.antlr.planLexer;
import guarana.compilador.backend.antlr.planParser;
import guarana.compilador.backend.componente.Translate;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.function.Consumer;

public final class Compilador {

    public Compilador(final Consumer<Translate> consumer) {
        this.consumer = consumer;
    }

    private final Consumer<Translate> consumer;

    public void traduzir(final String cadeia) throws CompileErrorException {

        final planParser planParser = new planParser(new CommonTokenStream(new planLexer(CharStreams.fromString(cadeia))));

        planParser.removeErrorListeners();

        planParser.addErrorListener(new ErrorParser());

        new ParseTreeWalker().walk(new planBaseListener(consumer, cadeia), planParser.plan_());

    }


    private static final class ErrorParser extends BaseErrorListener {

        @Override
        public void syntaxError
                (
                        final Recognizer<?, ?> r,
                        final Object o,
                        final int l,
                        final int c,
                        final String m,
                        final RecognitionException e) {
            throw new CompileErrorException(m, e);
        }

    }

}
