package guarana.compilador.backend;

import org.antlr.v4.runtime.RecognitionException;

public final class CompileErrorException extends RuntimeException {

    CompileErrorException(final String msg, final RecognitionException e) {
        super(msg, e);
    }

}
