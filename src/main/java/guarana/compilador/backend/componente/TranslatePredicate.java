package guarana.compilador.backend.componente;

interface TranslatePredicate {

    String generationCode(final String address, final int ticket, final Translate translate);

}
