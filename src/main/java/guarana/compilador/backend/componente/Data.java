package guarana.compilador.backend.componente;

public interface Data extends Comparable<Attribute> {

    String sectionUnit();

}
