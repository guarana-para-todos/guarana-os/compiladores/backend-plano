package guarana.compilador.backend.componente;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public final class Executor implements Translate {

    static Executor mock(final Iterator<String> registers) {
        return new Executor(List.of(Writer.mock(registers)), Driver.mock(1).addressIterable());
    }

    public Executor(final Collection<Writer> writers, final Collection<Address> addresses) {
        this.writers = writers;
        this.addresses = addresses;
    }

    private final Collection<Writer> writers;

    private final Collection<Address> addresses;

    @Override
    public String generationCode() {

        final StringBuilder builder = Address.code(addresses);

        builder.append("call _exec\n");

        builder.append(writers.stream().map(Writer::generationCode).collect(Collectors.joining()));

        return builder.toString();

    }

}
