package guarana.compilador.backend.componente;

import java.util.Collection;
import java.util.stream.Collectors;

enum SectionType implements TranslateSection {

    DATA {
        @Override
        public String generationCode(final Collection<Translate> translates) {
            return String.format
                    (
                            "section\t .data\n%s",
                            translates
                                    .stream()
                                    .map(Translate::generationCode)
                                    .collect(Collectors.joining("\n"))
                    );
        }
    },
    TEXT {
        @Override
        public String generationCode(final Collection<Translate> translates) {
            return String.format
                    (
                            "\n\nsection\t .text\n%s",
                            translates
                                    .stream()
                                    .map(Translate::generationCode)
                                    .collect(Collectors.joining())
                    );
        }
    }

}
