package guarana.compilador.backend.componente;

import java.util.Collection;

interface TranslateSection {

    String generationCode(final Collection<Translate> translates);

}
