package guarana.compilador.backend.componente;

import java.util.Arrays;
import java.util.stream.IntStream;

public enum Predicate implements TranslatePredicate {

    SUCESSO("success") {
        @Override
        public String generationCode(final String address, final int ticket, final Translate translate) {
            return String.format("cmp %s, [esp + %d]\njne %s\n%s\n%s:\t", "0x1", ticket, address, translate.generationCode(), address);
        }
    },
    FALHA("failed") {
        @Override
        public String generationCode(final String address, final int ticket, final Translate translate) {
            return String.format("cmp %s, [esp + %d]\njne %s\n%s\n%s:\t", "0x0", ticket, address, translate.generationCode(), address);
        }
    },
    SUCESSO_GLOBAL("success_global") {
        @Override
        public String generationCode(final String address, final int ticket, final Translate translate) {

            final StringBuilder code = builder(ticket);

            code.append(String.format("cmp %s, ax\njne %s\n%s\n%s:\t", "0x1", address, translate.generationCode(), address));

            return code.toString();

        }


    },
    FALHA_GLOBAL("failed_global") {
        @Override
        public String generationCode(final String address, final int ticket, final Translate translate) {

            final StringBuilder code = builder(ticket);

            code.append(String.format("cmp %s, ax\njne %s\n%s\n%s:\t", "0x0", address, translate.generationCode(), address));

            return code.toString();

        }

    };

    private static StringBuilder builder(int ticket) {

        final StringBuilder stringBuilder = new StringBuilder();

        boolean lock_mov = true;

        for (final Integer value : IntStream.rangeClosed(1, ticket).boxed().toList()) {
            stringBuilder.append(String.format("%s ax, [esp + %d]\n", lock_mov ? "mov" : "and", value));
            lock_mov = false;
        }

        return stringBuilder;

    }

    public static Predicate from(final String string) {
        return Arrays.stream(values()).filter(predicate -> predicate.value.equalsIgnoreCase(string))
                .findFirst()
                .orElseThrow();
    }

    Predicate(final String value) {
        this.value = value;
    }

    private final String value;

    String value() {
        return value;
    }

}
