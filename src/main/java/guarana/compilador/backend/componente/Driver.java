package guarana.compilador.backend.componente;

import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

final class Driver {

    static Driver mock(int ticket) {

        return new Driver
                (
                        List.of
                                (
                                        new Attribute(ticket, ticket, "nome", "valor"),
                                        new Attribute(ticket, ticket, "idade", "21")
                                )
                );

    }

    Driver(final Collection<Attribute> attributes) {
        this.attributes = attributes;
    }

    private final Collection<Attribute> attributes;

    public Collection<Address> addressIterable() {
        return new TreeSet<>(attributes);
    }

    public Collection<Data> dataIterable() {
        return new TreeSet<>(attributes);
    }

}
