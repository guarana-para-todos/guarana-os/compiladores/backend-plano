package guarana.compilador.backend.componente;

public final class Attribute implements Data, Address {

    static Attribute mock() {
        return new Attribute(1, 1, "nome", "nome");
    }

    public Attribute(final int executorIndex, final int resourceIndex, final String key, final String value) {
        this.executorIndex = executorIndex;
        this.resourceIndex = resourceIndex;
        this.key = key;
        this.value = value;
    }

    private final int resourceIndex;

    private final int executorIndex;

    private final String key;

    private final String value;

    @Override
    public String addressUnit() {
        return String.format("v%d%d_%s", executorIndex, resourceIndex, key.trim());
    }

    @Override
    public String sectionUnit() {
        return String.format("%s db \"%s\"", addressUnit(), value.trim().replaceAll("\"", ""));
    }

    @Override
    public int compareTo(final Attribute attribute) {
        return addressUnit().compareTo(attribute.addressUnit());
    }
}
