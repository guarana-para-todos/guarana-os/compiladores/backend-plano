package guarana.compilador.backend.componente;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class Component implements Translate {

    public Component
            (
                    final Plan plan,
                    final Collection<Executor> executors,
                    final Collection<Writer> writers,
                    final Collection<Data> datas
            ) {
        this.executors = executors;
        this.writers = writers;
        this.datas = datas;
        this.plan = plan;
    }

    private final Plan plan;

    private final Collection<Data> datas;

    private final Collection<Executor> executors;

    private final Collection<Writer> writers;

    @Override
    public String generationCode() {

        datas.addAll(plan.sectionDatas());

        final String sectionData = SectionType.DATA.generationCode
                (
                        datas
                                .stream()
                                .map(Data::sectionUnit)
                                .map(s -> (Translate) () -> s)
                                .collect(Collectors.toList())
                );

        final List<String> list = new ArrayList<>
                (
                        executors
                                .stream()
                                .map(Executor::generationCode)
                                .toList()
                );

        list.addAll(writers.stream().map(Writer::generationCode).toList());

        list.add(plan.generationCode());

        final String sectionText = SectionType.TEXT
                .generationCode
                        (
                                list
                                        .stream()
                                        .map(s -> (Translate) () -> s)
                                        .collect(Collectors.toList())
                        );

        return String.format("%s\n%s", sectionData, sectionText);

    }

}
