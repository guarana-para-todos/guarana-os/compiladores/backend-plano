package guarana.compilador.backend.componente;

import java.util.Collection;
import java.util.Iterator;

public final class Writer implements Translate {

    static Writer mock(final Iterator<String> registers) {
        return mock(1, registers, Predicate.SUCESSO);
    }

    static Writer mock(final int testNumber, final Iterator<String> registers, final Predicate predicado) {

        return new Writer
                (
                        testNumber,
                        Driver.mock(testNumber).addressIterable(),
                        predicado,
                        registers
                );

    }

    public Writer
            (
                    final int testNumber,
                    final Collection<Address> addresses,
                    final Predicate predicate,
                    final Iterator<String> registries
            ) {
        this.addresses = addresses;
        this.predicate = predicate;
        this.registries = registries;
        this.ticketTest = testNumber;
    }

    private final Collection<Address> addresses;

    private final Predicate predicate;

    private final Iterator<String> registries;

    private final int ticketTest;

    @Override
    public String generationCode() {

        final StringBuilder stringBuilder = Address.code(addresses);

        stringBuilder.append("call _write");

        return predicate.generationCode
                (
                        registries.next(),
                        ticketTest,
                        () -> stringBuilder.toString().trim()
                );

    }

}
