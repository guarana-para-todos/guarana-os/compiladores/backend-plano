package guarana.compilador.backend.componente;

import java.util.Base64;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class Plan implements Translate {

    public Plan
            (
                    final int testNumber,
                    final Iterator<String> registries,
                    final String nome,
                    final String raw,
                    final String driver
            ) {
        this.testNumber = testNumber;
        this.registries = registries;
        this.nome = nome;
        this.raw = Base64.getEncoder().encodeToString(raw.getBytes());
        this.driver = driver;
    }

    private final int testNumber;
    private final Iterator<String> registries;

    private final String nome;
    private final String raw;
    private final String driver;

    @Override
    public String generationCode() {

        final Collection<Address> addresses = toDriver().addressIterable();

        return new Writer(testNumber, addresses, Predicate.SUCESSO_GLOBAL, registries)
                .generationCode().concat("ret");

    }

    Collection<Data> sectionDatas() {
        return toDriver().dataIterable();
    }

    private Driver toDriver() {

        final Attribute driver = new Attribute(0, 0, "driver", this.driver);

        final Attribute plan = new Attribute(0, 0, String.format("plano_%s", nome), raw);

        return new Driver(List.of(driver, plan));

    }
}
