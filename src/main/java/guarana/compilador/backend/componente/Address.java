package guarana.compilador.backend.componente;

import java.util.Collection;

interface Address {

    String addressUnit();

    static StringBuilder code(final Collection<Address> addresses) {

        final StringBuilder stringBuilder = new StringBuilder();

        boolean lock_mov = true;

        for (Address address : addresses) {

            final String unit = address.addressUnit();

            stringBuilder.append(String.format("%s cx , %s\n", lock_mov ? "mov" : "add", unit));

            lock_mov = false;

        }

        return stringBuilder;

    }
}
