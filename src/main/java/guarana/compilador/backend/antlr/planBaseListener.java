// Generated from /home/lisboa/Documentos/Projetos/Compilador-Plano/src/main/resources/plan.g4 by ANTLR 4.12.0
package guarana.compilador.backend.antlr;

import guarana.compilador.backend.componente.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/**
 * This class provides an empty implementation of {@link planListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
@SuppressWarnings("CheckReturnValue")
public class planBaseListener implements planListener {

    public planBaseListener(final Consumer<Translate> consumer, final String planRaw) {
        this.planRaw = planRaw;
        this.registries =
                IntStream
                        .rangeClosed(65, 90)
                        .mapToObj(value -> (char) value)
                        .map(Object::toString)
                        .toList()
                        .iterator();
        this.consumer = consumer;
    }

    private final Consumer<Translate> consumer;

    private final Iterator<String> registries;

    private final String planRaw;

    private final AtomicInteger testAtomic = new AtomicInteger(0);

    private final AtomicInteger resourceAtomic = new AtomicInteger(0);

    private final Deque<Attribute> attributes = new ArrayDeque<>();

    private final Deque<Writer> notifiersTest = new ArrayDeque<>();

    private final Deque<Writer> notifiersGlobal = new ArrayDeque<>();

    private final Deque<Executor> executors = new ArrayDeque<>();

    private final Deque<Data> datas = new ArrayDeque<>();

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterPlan_(planParser.Plan_Context ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitPlan_(planParser.Plan_Context ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterComponent(planParser.ComponentContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitComponent(planParser.ComponentContext ctx) {
        final String name = ctx.id().ID_FRAGMENT().getText();
        final Plan plan = new Plan(testAtomic.get(), registries, name, planRaw, "disk_plan");
        consumer.accept
                (
                        new Component
                                (
                                        plan,
                                        new ArrayList<>(executors),
                                        new ArrayList<>(notifiersGlobal),
                                        new TreeSet<>(datas)
                                )
                );
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterTest(planParser.TestContext ctx) {
        testAtomic.accumulateAndGet(1, Integer::sum);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitTest(planParser.TestContext ctx) {
        executors.add(new Executor(new ArrayList<>(notifiersTest), new ArrayList<>(attributes)));
        notifiersTest.clear();
        attributes.clear();
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterNotifier_global(planParser.Notifier_globalContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitNotifier_global(planParser.Notifier_globalContext ctx) {
        final String name = String.format("%s_global", ctx.predicate().value().getText());

        notifiersGlobal
                .add
                        (
                                new Writer
                                        (
                                                testAtomic.get(),
                                                new ArrayList<>(attributes),
                                                Predicate.from(name),
                                                registries
                                        )
                        );

        attributes.clear();

    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterNotifier(planParser.NotifierContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitNotifier(planParser.NotifierContext ctx) {
        notifiersTest
                .add
                        (
                                new Writer
                                        (
                                                testAtomic.get(),
                                                new ArrayList<>(attributes),
                                                Predicate.from(ctx.predicate().value().getText()),
                                                registries
                                        )
                        );
        attributes.clear();
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterExpressao(planParser.ExpressaoContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitExpressao(planParser.ExpressaoContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterDriver(planParser.DriverContext ctx) {
        resourceAtomic.accumulateAndGet(1, Integer::sum);
        final String key = "driver";
        final String value = ctx.id().ID_FRAGMENT().getText();
        final Attribute attribute = new Attribute(testAtomic.get(), resourceAtomic.get(), key, value);
        attributes.add(attribute);
        datas.add(attribute);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitDriver(planParser.DriverContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterPredicate(planParser.PredicateContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitPredicate(planParser.PredicateContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterValue(planParser.ValueContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitValue(planParser.ValueContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterId(planParser.IdContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitId(planParser.IdContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterData(planParser.DataContext ctx) {
        final String key = ctx.attribute().ID_FRAGMENT().getText();
        final String value = ctx.string().STRING().getText();
        final Attribute attribute = new Attribute(testAtomic.get(), resourceAtomic.get(), key, value);
        attributes.add(attribute);
        datas.add(attribute);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitData(planParser.DataContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterFile(planParser.FileContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitFile(planParser.FileContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterAttribute(planParser.AttributeContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitAttribute(planParser.AttributeContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterString(planParser.StringContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitString(planParser.StringContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void visitTerminal(TerminalNode node) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void visitErrorNode(ErrorNode node) {
    }
}