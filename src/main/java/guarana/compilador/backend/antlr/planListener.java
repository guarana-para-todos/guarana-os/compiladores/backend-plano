// Generated from /home/lisboa/Documentos/Projetos/Compilador-Plano/src/main/resources/plan.g4 by ANTLR 4.12.0
package guarana.compilador.backend.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link planParser}.
 */
public interface planListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link planParser#plan_}.
	 * @param ctx the parse tree
	 */
	void enterPlan_(planParser.Plan_Context ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#plan_}.
	 * @param ctx the parse tree
	 */
	void exitPlan_(planParser.Plan_Context ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#component}.
	 * @param ctx the parse tree
	 */
	void enterComponent(planParser.ComponentContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#component}.
	 * @param ctx the parse tree
	 */
	void exitComponent(planParser.ComponentContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#test}.
	 * @param ctx the parse tree
	 */
	void enterTest(planParser.TestContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#test}.
	 * @param ctx the parse tree
	 */
	void exitTest(planParser.TestContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#notifier_global}.
	 * @param ctx the parse tree
	 */
	void enterNotifier_global(planParser.Notifier_globalContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#notifier_global}.
	 * @param ctx the parse tree
	 */
	void exitNotifier_global(planParser.Notifier_globalContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#notifier}.
	 * @param ctx the parse tree
	 */
	void enterNotifier(planParser.NotifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#notifier}.
	 * @param ctx the parse tree
	 */
	void exitNotifier(planParser.NotifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#expressao}.
	 * @param ctx the parse tree
	 */
	void enterExpressao(planParser.ExpressaoContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#expressao}.
	 * @param ctx the parse tree
	 */
	void exitExpressao(planParser.ExpressaoContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#driver}.
	 * @param ctx the parse tree
	 */
	void enterDriver(planParser.DriverContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#driver}.
	 * @param ctx the parse tree
	 */
	void exitDriver(planParser.DriverContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(planParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(planParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(planParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(planParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#id}.
	 * @param ctx the parse tree
	 */
	void enterId(planParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#id}.
	 * @param ctx the parse tree
	 */
	void exitId(planParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#data}.
	 * @param ctx the parse tree
	 */
	void enterData(planParser.DataContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#data}.
	 * @param ctx the parse tree
	 */
	void exitData(planParser.DataContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#file}.
	 * @param ctx the parse tree
	 */
	void enterFile(planParser.FileContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#file}.
	 * @param ctx the parse tree
	 */
	void exitFile(planParser.FileContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(planParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(planParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link planParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(planParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link planParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(planParser.StringContext ctx);
}