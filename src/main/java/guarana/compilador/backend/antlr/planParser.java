// Generated from /home/lisboa/Documentos/Projetos/Compilador-Plano/src/main/resources/plan.g4 by ANTLR 4.12.0
package guarana.compilador.backend.antlr;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class planParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.12.0", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, STRING=9, 
		ID_FRAGMENT=10, COLCHETE_INICIAL=11, COLCHETE_FINAL=12, PARENTESE_INICIAL=13, 
		PARENTESE_FINAL=14, WS=15;
	public static final int
		RULE_plan_ = 0, RULE_component = 1, RULE_test = 2, RULE_notifier_global = 3, 
		RULE_notifier = 4, RULE_expressao = 5, RULE_driver = 6, RULE_predicate = 7, 
		RULE_value = 8, RULE_id = 9, RULE_data = 10, RULE_file = 11, RULE_attribute = 12, 
		RULE_string = 13;
	private static String[] makeRuleNames() {
		return new String[] {
			"plan_", "component", "test", "notifier_global", "notifier", "expressao", 
			"driver", "predicate", "value", "id", "data", "file", "attribute", "string"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'component'", "'test'", "'notifier'", "'driver'", "'success'", 
			"'failed'", "'file'", "':'", null, null, "'['", "']'", "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "STRING", "ID_FRAGMENT", 
			"COLCHETE_INICIAL", "COLCHETE_FINAL", "PARENTESE_INICIAL", "PARENTESE_FINAL", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "plan.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public planParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Plan_Context extends ParserRuleContext {
		public ComponentContext component() {
			return getRuleContext(ComponentContext.class,0);
		}
		public Plan_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plan_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterPlan_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitPlan_(this);
		}
	}

	public final Plan_Context plan_() throws RecognitionException {
		Plan_Context _localctx = new Plan_Context(_ctx, getState());
		enterRule(_localctx, 0, RULE_plan_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			component();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ComponentContext extends ParserRuleContext {
		public TerminalNode PARENTESE_INICIAL() { return getToken(planParser.PARENTESE_INICIAL, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode PARENTESE_FINAL() { return getToken(planParser.PARENTESE_FINAL, 0); }
		public List<TestContext> test() {
			return getRuleContexts(TestContext.class);
		}
		public TestContext test(int i) {
			return getRuleContext(TestContext.class,i);
		}
		public List<Notifier_globalContext> notifier_global() {
			return getRuleContexts(Notifier_globalContext.class);
		}
		public Notifier_globalContext notifier_global(int i) {
			return getRuleContext(Notifier_globalContext.class,i);
		}
		public ComponentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_component; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterComponent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitComponent(this);
		}
	}

	public final ComponentContext component() throws RecognitionException {
		ComponentContext _localctx = new ComponentContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_component);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(30);
			match(PARENTESE_INICIAL);
			setState(31);
			match(T__0);
			setState(32);
			id();
			setState(34); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(33);
					test();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(36); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(41);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PARENTESE_INICIAL) {
				{
				{
				setState(38);
				notifier_global();
				}
				}
				setState(43);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(44);
			match(PARENTESE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TestContext extends ParserRuleContext {
		public TerminalNode PARENTESE_INICIAL() { return getToken(planParser.PARENTESE_INICIAL, 0); }
		public ExpressaoContext expressao() {
			return getRuleContext(ExpressaoContext.class,0);
		}
		public TerminalNode PARENTESE_FINAL() { return getToken(planParser.PARENTESE_FINAL, 0); }
		public TestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_test; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterTest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitTest(this);
		}
	}

	public final TestContext test() throws RecognitionException {
		TestContext _localctx = new TestContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_test);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(PARENTESE_INICIAL);
			setState(47);
			match(T__1);
			setState(48);
			expressao();
			setState(49);
			match(PARENTESE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Notifier_globalContext extends ParserRuleContext {
		public TerminalNode PARENTESE_INICIAL() { return getToken(planParser.PARENTESE_INICIAL, 0); }
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public DriverContext driver() {
			return getRuleContext(DriverContext.class,0);
		}
		public TerminalNode PARENTESE_FINAL() { return getToken(planParser.PARENTESE_FINAL, 0); }
		public Notifier_globalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notifier_global; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterNotifier_global(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitNotifier_global(this);
		}
	}

	public final Notifier_globalContext notifier_global() throws RecognitionException {
		Notifier_globalContext _localctx = new Notifier_globalContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_notifier_global);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			match(PARENTESE_INICIAL);
			setState(52);
			match(T__2);
			setState(53);
			predicate();
			setState(54);
			driver();
			setState(55);
			match(PARENTESE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NotifierContext extends ParserRuleContext {
		public TerminalNode PARENTESE_INICIAL() { return getToken(planParser.PARENTESE_INICIAL, 0); }
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public DriverContext driver() {
			return getRuleContext(DriverContext.class,0);
		}
		public TerminalNode PARENTESE_FINAL() { return getToken(planParser.PARENTESE_FINAL, 0); }
		public NotifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterNotifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitNotifier(this);
		}
	}

	public final NotifierContext notifier() throws RecognitionException {
		NotifierContext _localctx = new NotifierContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_notifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			match(PARENTESE_INICIAL);
			setState(58);
			match(T__2);
			setState(59);
			predicate();
			setState(60);
			driver();
			setState(61);
			match(PARENTESE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExpressaoContext extends ParserRuleContext {
		public DriverContext driver() {
			return getRuleContext(DriverContext.class,0);
		}
		public List<NotifierContext> notifier() {
			return getRuleContexts(NotifierContext.class);
		}
		public NotifierContext notifier(int i) {
			return getRuleContext(NotifierContext.class,i);
		}
		public ExpressaoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressao; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterExpressao(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitExpressao(this);
		}
	}

	public final ExpressaoContext expressao() throws RecognitionException {
		ExpressaoContext _localctx = new ExpressaoContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_expressao);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(63);
					notifier();
					}
					} 
				}
				setState(68);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(69);
			driver();
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PARENTESE_INICIAL) {
				{
				{
				setState(70);
				notifier();
				}
				}
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DriverContext extends ParserRuleContext {
		public TerminalNode PARENTESE_INICIAL() { return getToken(planParser.PARENTESE_INICIAL, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode PARENTESE_FINAL() { return getToken(planParser.PARENTESE_FINAL, 0); }
		public List<DataContext> data() {
			return getRuleContexts(DataContext.class);
		}
		public DataContext data(int i) {
			return getRuleContext(DataContext.class,i);
		}
		public DriverContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_driver; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterDriver(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitDriver(this);
		}
	}

	public final DriverContext driver() throws RecognitionException {
		DriverContext _localctx = new DriverContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_driver);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(PARENTESE_INICIAL);
			setState(77);
			match(T__3);
			setState(78);
			id();
			setState(80); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(79);
				data();
				}
				}
				setState(82); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__7 );
			setState(84);
			match(PARENTESE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PredicateContext extends ParserRuleContext {
		public TerminalNode COLCHETE_INICIAL() { return getToken(planParser.COLCHETE_INICIAL, 0); }
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public TerminalNode COLCHETE_FINAL() { return getToken(planParser.COLCHETE_FINAL, 0); }
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterPredicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitPredicate(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_predicate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			match(COLCHETE_INICIAL);
			setState(87);
			value();
			setState(88);
			match(COLCHETE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			_la = _input.LA(1);
			if ( !(_la==T__4 || _la==T__5) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IdContext extends ParserRuleContext {
		public TerminalNode ID_FRAGMENT() { return getToken(planParser.ID_FRAGMENT, 0); }
		public IdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitId(this);
		}
	}

	public final IdContext id() throws RecognitionException {
		IdContext _localctx = new IdContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(ID_FRAGMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DataContext extends ParserRuleContext {
		public AttributeContext attribute() {
			return getRuleContext(AttributeContext.class,0);
		}
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public FileContext file() {
			return getRuleContext(FileContext.class,0);
		}
		public DataContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_data; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterData(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitData(this);
		}
	}

	public final DataContext data() throws RecognitionException {
		DataContext _localctx = new DataContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_data);
		try {
			setState(100);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(94);
				attribute();
				setState(95);
				string();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(97);
				attribute();
				setState(98);
				file();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FileContext extends ParserRuleContext {
		public TerminalNode PARENTESE_INICIAL() { return getToken(planParser.PARENTESE_INICIAL, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode PARENTESE_FINAL() { return getToken(planParser.PARENTESE_FINAL, 0); }
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterFile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitFile(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_file);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			match(PARENTESE_INICIAL);
			setState(103);
			match(T__6);
			setState(104);
			string();
			setState(105);
			match(PARENTESE_FINAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AttributeContext extends ParserRuleContext {
		public TerminalNode ID_FRAGMENT() { return getToken(planParser.ID_FRAGMENT, 0); }
		public AttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitAttribute(this);
		}
	}

	public final AttributeContext attribute() throws RecognitionException {
		AttributeContext _localctx = new AttributeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_attribute);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(T__7);
			setState(108);
			match(ID_FRAGMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StringContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(planParser.STRING, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof planListener ) ((planListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_string);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\u0004\u0001\u000fq\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0004\u0001#\b\u0001\u000b\u0001\f\u0001$\u0001"+
		"\u0001\u0005\u0001(\b\u0001\n\u0001\f\u0001+\t\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001"+
		"\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001"+
		"\u0005\u0005\u0005A\b\u0005\n\u0005\f\u0005D\t\u0005\u0001\u0005\u0001"+
		"\u0005\u0005\u0005H\b\u0005\n\u0005\f\u0005K\t\u0005\u0001\u0006\u0001"+
		"\u0006\u0001\u0006\u0001\u0006\u0004\u0006Q\b\u0006\u000b\u0006\f\u0006"+
		"R\u0001\u0006\u0001\u0006\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007"+
		"\u0001\b\u0001\b\u0001\t\u0001\t\u0001\n\u0001\n\u0001\n\u0001\n\u0001"+
		"\n\u0001\n\u0003\ne\b\n\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b"+
		"\u0001\u000b\u0001\f\u0001\f\u0001\f\u0001\r\u0001\r\u0001\r\u0000\u0000"+
		"\u000e\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018"+
		"\u001a\u0000\u0001\u0001\u0000\u0005\u0006h\u0000\u001c\u0001\u0000\u0000"+
		"\u0000\u0002\u001e\u0001\u0000\u0000\u0000\u0004.\u0001\u0000\u0000\u0000"+
		"\u00063\u0001\u0000\u0000\u0000\b9\u0001\u0000\u0000\u0000\nB\u0001\u0000"+
		"\u0000\u0000\fL\u0001\u0000\u0000\u0000\u000eV\u0001\u0000\u0000\u0000"+
		"\u0010Z\u0001\u0000\u0000\u0000\u0012\\\u0001\u0000\u0000\u0000\u0014"+
		"d\u0001\u0000\u0000\u0000\u0016f\u0001\u0000\u0000\u0000\u0018k\u0001"+
		"\u0000\u0000\u0000\u001an\u0001\u0000\u0000\u0000\u001c\u001d\u0003\u0002"+
		"\u0001\u0000\u001d\u0001\u0001\u0000\u0000\u0000\u001e\u001f\u0005\r\u0000"+
		"\u0000\u001f \u0005\u0001\u0000\u0000 \"\u0003\u0012\t\u0000!#\u0003\u0004"+
		"\u0002\u0000\"!\u0001\u0000\u0000\u0000#$\u0001\u0000\u0000\u0000$\"\u0001"+
		"\u0000\u0000\u0000$%\u0001\u0000\u0000\u0000%)\u0001\u0000\u0000\u0000"+
		"&(\u0003\u0006\u0003\u0000\'&\u0001\u0000\u0000\u0000(+\u0001\u0000\u0000"+
		"\u0000)\'\u0001\u0000\u0000\u0000)*\u0001\u0000\u0000\u0000*,\u0001\u0000"+
		"\u0000\u0000+)\u0001\u0000\u0000\u0000,-\u0005\u000e\u0000\u0000-\u0003"+
		"\u0001\u0000\u0000\u0000./\u0005\r\u0000\u0000/0\u0005\u0002\u0000\u0000"+
		"01\u0003\n\u0005\u000012\u0005\u000e\u0000\u00002\u0005\u0001\u0000\u0000"+
		"\u000034\u0005\r\u0000\u000045\u0005\u0003\u0000\u000056\u0003\u000e\u0007"+
		"\u000067\u0003\f\u0006\u000078\u0005\u000e\u0000\u00008\u0007\u0001\u0000"+
		"\u0000\u00009:\u0005\r\u0000\u0000:;\u0005\u0003\u0000\u0000;<\u0003\u000e"+
		"\u0007\u0000<=\u0003\f\u0006\u0000=>\u0005\u000e\u0000\u0000>\t\u0001"+
		"\u0000\u0000\u0000?A\u0003\b\u0004\u0000@?\u0001\u0000\u0000\u0000AD\u0001"+
		"\u0000\u0000\u0000B@\u0001\u0000\u0000\u0000BC\u0001\u0000\u0000\u0000"+
		"CE\u0001\u0000\u0000\u0000DB\u0001\u0000\u0000\u0000EI\u0003\f\u0006\u0000"+
		"FH\u0003\b\u0004\u0000GF\u0001\u0000\u0000\u0000HK\u0001\u0000\u0000\u0000"+
		"IG\u0001\u0000\u0000\u0000IJ\u0001\u0000\u0000\u0000J\u000b\u0001\u0000"+
		"\u0000\u0000KI\u0001\u0000\u0000\u0000LM\u0005\r\u0000\u0000MN\u0005\u0004"+
		"\u0000\u0000NP\u0003\u0012\t\u0000OQ\u0003\u0014\n\u0000PO\u0001\u0000"+
		"\u0000\u0000QR\u0001\u0000\u0000\u0000RP\u0001\u0000\u0000\u0000RS\u0001"+
		"\u0000\u0000\u0000ST\u0001\u0000\u0000\u0000TU\u0005\u000e\u0000\u0000"+
		"U\r\u0001\u0000\u0000\u0000VW\u0005\u000b\u0000\u0000WX\u0003\u0010\b"+
		"\u0000XY\u0005\f\u0000\u0000Y\u000f\u0001\u0000\u0000\u0000Z[\u0007\u0000"+
		"\u0000\u0000[\u0011\u0001\u0000\u0000\u0000\\]\u0005\n\u0000\u0000]\u0013"+
		"\u0001\u0000\u0000\u0000^_\u0003\u0018\f\u0000_`\u0003\u001a\r\u0000`"+
		"e\u0001\u0000\u0000\u0000ab\u0003\u0018\f\u0000bc\u0003\u0016\u000b\u0000"+
		"ce\u0001\u0000\u0000\u0000d^\u0001\u0000\u0000\u0000da\u0001\u0000\u0000"+
		"\u0000e\u0015\u0001\u0000\u0000\u0000fg\u0005\r\u0000\u0000gh\u0005\u0007"+
		"\u0000\u0000hi\u0003\u001a\r\u0000ij\u0005\u000e\u0000\u0000j\u0017\u0001"+
		"\u0000\u0000\u0000kl\u0005\b\u0000\u0000lm\u0005\n\u0000\u0000m\u0019"+
		"\u0001\u0000\u0000\u0000no\u0005\t\u0000\u0000o\u001b\u0001\u0000\u0000"+
		"\u0000\u0006$)BIRd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}