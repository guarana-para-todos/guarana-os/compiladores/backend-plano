// Generated from /home/lisboa/Documentos/Projetos/Compilador-Plano/src/main/resources/plan.g4 by ANTLR 4.12.0
package guarana.compilador.backend.antlr;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class planLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.12.0", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, STRING=9, 
		ID_FRAGMENT=10, COLCHETE_INICIAL=11, COLCHETE_FINAL=12, PARENTESE_INICIAL=13, 
		PARENTESE_FINAL=14, WS=15;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "STRING", 
			"DOUBLE_QUOTE_CHAR", "SINGLE_QUOTE_CHAR", "ESCAPE_SEQUENCE", "UNICODE_SEQUENCE", 
			"HEX", "NEWLINE", "ID_FRAGMENT", "COLCHETE_INICIAL", "COLCHETE_FINAL", 
			"PARENTESE_INICIAL", "PARENTESE_FINAL", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'component'", "'test'", "'notifier'", "'driver'", "'success'", 
			"'failed'", "'file'", "':'", null, null, "'['", "']'", "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "STRING", "ID_FRAGMENT", 
			"COLCHETE_INICIAL", "COLCHETE_FINAL", "PARENTESE_INICIAL", "PARENTESE_FINAL", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public planLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "plan.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\u0004\u0000\u000f\u00a7\u0006\uffff\uffff\u0002\u0000\u0007\u0000\u0002"+
		"\u0001\u0007\u0001\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002"+
		"\u0004\u0007\u0004\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002"+
		"\u0007\u0007\u0007\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002"+
		"\u000b\u0007\u000b\u0002\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e"+
		"\u0002\u000f\u0007\u000f\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011"+
		"\u0002\u0012\u0007\u0012\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014"+
		"\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000"+
		"\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005\u0001\u0005"+
		"\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0006\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0007\u0001\u0007\u0001\b\u0001"+
		"\b\u0005\bc\b\b\n\b\f\bf\t\b\u0001\b\u0001\b\u0001\b\u0005\bk\b\b\n\b"+
		"\f\bn\t\b\u0001\b\u0003\bq\b\b\u0001\t\u0001\t\u0003\tu\b\t\u0001\n\u0001"+
		"\n\u0003\ny\b\n\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001"+
		"\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0003"+
		"\u000b\u0085\b\u000b\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001"+
		"\r\u0001\r\u0001\u000e\u0001\u000e\u0001\u000e\u0003\u000e\u0092\b\u000e"+
		"\u0001\u000f\u0004\u000f\u0095\b\u000f\u000b\u000f\f\u000f\u0096\u0001"+
		"\u0010\u0001\u0010\u0001\u0011\u0001\u0011\u0001\u0012\u0001\u0012\u0001"+
		"\u0013\u0001\u0013\u0001\u0014\u0004\u0014\u00a2\b\u0014\u000b\u0014\f"+
		"\u0014\u00a3\u0001\u0014\u0001\u0014\u0000\u0000\u0015\u0001\u0001\u0003"+
		"\u0002\u0005\u0003\u0007\u0004\t\u0005\u000b\u0006\r\u0007\u000f\b\u0011"+
		"\t\u0013\u0000\u0015\u0000\u0017\u0000\u0019\u0000\u001b\u0000\u001d\u0000"+
		"\u001f\n!\u000b#\f%\r\'\u000e)\u000f\u0001\u0000\b\u0004\u0000\n\n\r\r"+
		"\"\"\\\\\u0004\u0000\n\n\r\r\'\'\\\\\n\u0000\"\"\'\'//\\\\bbffnnrrttv"+
		"v\f\u0000\n\n\r\r\"\"\'\'09\\\\bbffnnrrtvxx\u0003\u000009AFaf\u0003\u0000"+
		"\n\n\r\r\u2028\u2029\u0002\u0000AZaz\u0003\u0000\t\n\r\r  \u00ad\u0000"+
		"\u0001\u0001\u0000\u0000\u0000\u0000\u0003\u0001\u0000\u0000\u0000\u0000"+
		"\u0005\u0001\u0000\u0000\u0000\u0000\u0007\u0001\u0000\u0000\u0000\u0000"+
		"\t\u0001\u0000\u0000\u0000\u0000\u000b\u0001\u0000\u0000\u0000\u0000\r"+
		"\u0001\u0000\u0000\u0000\u0000\u000f\u0001\u0000\u0000\u0000\u0000\u0011"+
		"\u0001\u0000\u0000\u0000\u0000\u001f\u0001\u0000\u0000\u0000\u0000!\u0001"+
		"\u0000\u0000\u0000\u0000#\u0001\u0000\u0000\u0000\u0000%\u0001\u0000\u0000"+
		"\u0000\u0000\'\u0001\u0000\u0000\u0000\u0000)\u0001\u0000\u0000\u0000"+
		"\u0001+\u0001\u0000\u0000\u0000\u00035\u0001\u0000\u0000\u0000\u0005:"+
		"\u0001\u0000\u0000\u0000\u0007C\u0001\u0000\u0000\u0000\tJ\u0001\u0000"+
		"\u0000\u0000\u000bR\u0001\u0000\u0000\u0000\rY\u0001\u0000\u0000\u0000"+
		"\u000f^\u0001\u0000\u0000\u0000\u0011p\u0001\u0000\u0000\u0000\u0013t"+
		"\u0001\u0000\u0000\u0000\u0015x\u0001\u0000\u0000\u0000\u0017z\u0001\u0000"+
		"\u0000\u0000\u0019\u0086\u0001\u0000\u0000\u0000\u001b\u008c\u0001\u0000"+
		"\u0000\u0000\u001d\u0091\u0001\u0000\u0000\u0000\u001f\u0094\u0001\u0000"+
		"\u0000\u0000!\u0098\u0001\u0000\u0000\u0000#\u009a\u0001\u0000\u0000\u0000"+
		"%\u009c\u0001\u0000\u0000\u0000\'\u009e\u0001\u0000\u0000\u0000)\u00a1"+
		"\u0001\u0000\u0000\u0000+,\u0005c\u0000\u0000,-\u0005o\u0000\u0000-.\u0005"+
		"m\u0000\u0000./\u0005p\u0000\u0000/0\u0005o\u0000\u000001\u0005n\u0000"+
		"\u000012\u0005e\u0000\u000023\u0005n\u0000\u000034\u0005t\u0000\u0000"+
		"4\u0002\u0001\u0000\u0000\u000056\u0005t\u0000\u000067\u0005e\u0000\u0000"+
		"78\u0005s\u0000\u000089\u0005t\u0000\u00009\u0004\u0001\u0000\u0000\u0000"+
		":;\u0005n\u0000\u0000;<\u0005o\u0000\u0000<=\u0005t\u0000\u0000=>\u0005"+
		"i\u0000\u0000>?\u0005f\u0000\u0000?@\u0005i\u0000\u0000@A\u0005e\u0000"+
		"\u0000AB\u0005r\u0000\u0000B\u0006\u0001\u0000\u0000\u0000CD\u0005d\u0000"+
		"\u0000DE\u0005r\u0000\u0000EF\u0005i\u0000\u0000FG\u0005v\u0000\u0000"+
		"GH\u0005e\u0000\u0000HI\u0005r\u0000\u0000I\b\u0001\u0000\u0000\u0000"+
		"JK\u0005s\u0000\u0000KL\u0005u\u0000\u0000LM\u0005c\u0000\u0000MN\u0005"+
		"c\u0000\u0000NO\u0005e\u0000\u0000OP\u0005s\u0000\u0000PQ\u0005s\u0000"+
		"\u0000Q\n\u0001\u0000\u0000\u0000RS\u0005f\u0000\u0000ST\u0005a\u0000"+
		"\u0000TU\u0005i\u0000\u0000UV\u0005l\u0000\u0000VW\u0005e\u0000\u0000"+
		"WX\u0005d\u0000\u0000X\f\u0001\u0000\u0000\u0000YZ\u0005f\u0000\u0000"+
		"Z[\u0005i\u0000\u0000[\\\u0005l\u0000\u0000\\]\u0005e\u0000\u0000]\u000e"+
		"\u0001\u0000\u0000\u0000^_\u0005:\u0000\u0000_\u0010\u0001\u0000\u0000"+
		"\u0000`d\u0005\"\u0000\u0000ac\u0003\u0013\t\u0000ba\u0001\u0000\u0000"+
		"\u0000cf\u0001\u0000\u0000\u0000db\u0001\u0000\u0000\u0000de\u0001\u0000"+
		"\u0000\u0000eg\u0001\u0000\u0000\u0000fd\u0001\u0000\u0000\u0000gq\u0005"+
		"\"\u0000\u0000hl\u0005\'\u0000\u0000ik\u0003\u0015\n\u0000ji\u0001\u0000"+
		"\u0000\u0000kn\u0001\u0000\u0000\u0000lj\u0001\u0000\u0000\u0000lm\u0001"+
		"\u0000\u0000\u0000mo\u0001\u0000\u0000\u0000nl\u0001\u0000\u0000\u0000"+
		"oq\u0005\'\u0000\u0000p`\u0001\u0000\u0000\u0000ph\u0001\u0000\u0000\u0000"+
		"q\u0012\u0001\u0000\u0000\u0000ru\b\u0000\u0000\u0000su\u0003\u0017\u000b"+
		"\u0000tr\u0001\u0000\u0000\u0000ts\u0001\u0000\u0000\u0000u\u0014\u0001"+
		"\u0000\u0000\u0000vy\b\u0001\u0000\u0000wy\u0003\u0017\u000b\u0000xv\u0001"+
		"\u0000\u0000\u0000xw\u0001\u0000\u0000\u0000y\u0016\u0001\u0000\u0000"+
		"\u0000z\u0084\u0005\\\u0000\u0000{\u0085\u0003\u001d\u000e\u0000|\u0085"+
		"\u0003\u0019\f\u0000}\u0085\u0007\u0002\u0000\u0000~\u0085\b\u0003\u0000"+
		"\u0000\u007f\u0085\u00050\u0000\u0000\u0080\u0081\u0005x\u0000\u0000\u0081"+
		"\u0082\u0003\u001b\r\u0000\u0082\u0083\u0003\u001b\r\u0000\u0083\u0085"+
		"\u0001\u0000\u0000\u0000\u0084{\u0001\u0000\u0000\u0000\u0084|\u0001\u0000"+
		"\u0000\u0000\u0084}\u0001\u0000\u0000\u0000\u0084~\u0001\u0000\u0000\u0000"+
		"\u0084\u007f\u0001\u0000\u0000\u0000\u0084\u0080\u0001\u0000\u0000\u0000"+
		"\u0085\u0018\u0001\u0000\u0000\u0000\u0086\u0087\u0005u\u0000\u0000\u0087"+
		"\u0088\u0003\u001b\r\u0000\u0088\u0089\u0003\u001b\r\u0000\u0089\u008a"+
		"\u0003\u001b\r\u0000\u008a\u008b\u0003\u001b\r\u0000\u008b\u001a\u0001"+
		"\u0000\u0000\u0000\u008c\u008d\u0007\u0004\u0000\u0000\u008d\u001c\u0001"+
		"\u0000\u0000\u0000\u008e\u008f\u0005\r\u0000\u0000\u008f\u0092\u0005\n"+
		"\u0000\u0000\u0090\u0092\u0007\u0005\u0000\u0000\u0091\u008e\u0001\u0000"+
		"\u0000\u0000\u0091\u0090\u0001\u0000\u0000\u0000\u0092\u001e\u0001\u0000"+
		"\u0000\u0000\u0093\u0095\u0007\u0006\u0000\u0000\u0094\u0093\u0001\u0000"+
		"\u0000\u0000\u0095\u0096\u0001\u0000\u0000\u0000\u0096\u0094\u0001\u0000"+
		"\u0000\u0000\u0096\u0097\u0001\u0000\u0000\u0000\u0097 \u0001\u0000\u0000"+
		"\u0000\u0098\u0099\u0005[\u0000\u0000\u0099\"\u0001\u0000\u0000\u0000"+
		"\u009a\u009b\u0005]\u0000\u0000\u009b$\u0001\u0000\u0000\u0000\u009c\u009d"+
		"\u0005(\u0000\u0000\u009d&\u0001\u0000\u0000\u0000\u009e\u009f\u0005)"+
		"\u0000\u0000\u009f(\u0001\u0000\u0000\u0000\u00a0\u00a2\u0007\u0007\u0000"+
		"\u0000\u00a1\u00a0\u0001\u0000\u0000\u0000\u00a2\u00a3\u0001\u0000\u0000"+
		"\u0000\u00a3\u00a1\u0001\u0000\u0000\u0000\u00a3\u00a4\u0001\u0000\u0000"+
		"\u0000\u00a4\u00a5\u0001\u0000\u0000\u0000\u00a5\u00a6\u0006\u0014\u0000"+
		"\u0000\u00a6*\u0001\u0000\u0000\u0000\n\u0000dlptx\u0084\u0091\u0096\u00a3"+
		"\u0001\u0006\u0000\u0000";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}