section	 .data
v00_driver db "disk_plan"
v00_plano_X db "KGNvbXBvbmVudCBYICAgICh0ZXN0ICAgICAgICAobm90aWZpZXIgW3N1Y2Nlc3NdIChkcml2ZXIgZW1haWwgOmVtYWlsICJ0ZXN0XzAxQGVtYWlsLmNvbSIpICkgICAgICAgIChkcml2ZXIgbmV3bWFuICAgICAgICAgICAgOmNvbGxlY3Rpb24gInRlc3RfMDEiICAgICAgICApICAgICkgICAgKHRlc3QgICAgICAgICAgICAobm90aWZpZXIgW3N1Y2Nlc3NdIChkcml2ZXIgZW1haWwgOmVtYWlsICJ0ZXN0XzAyQGVtYWlsLmNvbSIpICkgICAgICAgICAgICAoZHJpdmVyIGN1cmwgICAgICAgICAgICAgICAgOmNvbGxlY3Rpb24gInRlc3RfMDIiICAgICAgICAgICAgKSAgICAgICApICAgKG5vdGlmaWVyIFtmYWlsZWRdIChkcml2ZXIgZW1haWwgOmVtYWlsICJnbG9iYWxAZW1haWwuY29tIikgKSk="
v11_driver db "email"
v11_email db "test_01@email.com"
v12_collection db "test_01"
v12_driver db "newman"
v23_driver db "email"
v23_email db "test_02@email.com"
v24_collection db "test_02"
v24_driver db "curl"
v25_driver db "email"
v25_email db "global@email.com"


section	 .text
mov cx , v12_driver
add cx , v12_collection
call _exec
cmp 0x1, [esp + 1]
jne A
mov cx , v11_driver
add cx , v11_email
call _write
A:	mov cx , v24_driver
add cx , v24_collection
call _exec
cmp 0x1, [esp + 2]
jne B
mov cx , v23_driver
add cx , v23_email
call _write
B:	mov ax, [esp + 1]
and ax, [esp + 2]
cmp 0x0, ax
jne C
mov cx , v25_driver
add cx , v25_email
call _write
C:	mov ax, [esp + 1]
and ax, [esp + 2]
cmp 0x1, ax
jne D
mov cx , v00_driver
add cx , v00_plano_X
call _write
D:	ret