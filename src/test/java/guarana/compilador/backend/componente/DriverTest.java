package guarana.compilador.backend.componente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

class DriverTest {

    @org.junit.jupiter.api.Test
    @DisplayName("Deve retornar o endereco da variavel")
    void deveRetornarEnderecoDaVariavel() {
        final Driver driver = new Driver(List.of(Attribute.mock()));
        Assertions.assertEquals("v11_nome", driver.addressIterable().iterator().next().addressUnit());
    }

    @Test
    @DisplayName("Deve retornar unidade da seção")
    void deveRetornarUnidadeDaSecao() {
        final Driver driver = new Driver(List.of(new Attribute(2, 3, "collection", "123")));
        Assertions.assertEquals("v23_collection db \"123\"", driver.dataIterable().iterator().next().sectionUnit());
    }

}