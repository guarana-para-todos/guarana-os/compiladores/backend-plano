package guarana.compilador.backend.componente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

class ExecutorTest {

    @Test
    @DisplayName("Deve traduzir um teste para código de maquina")
    void deveTraduzirTesteParaCodigoDeMaquina() {

        final Executor executor = Executor.mock(List.of("A", "B").iterator());

        Assertions.assertEquals
                (
                        """
                                mov cx , v11_idade
                                add cx , v11_nome
                                call _exec
                                cmp 0x1, [esp + 1]
                                jne A
                                mov cx , v11_idade
                                add cx , v11_nome
                                call _write
                                A:""",
                        executor.generationCode().trim()
                );

    }
}