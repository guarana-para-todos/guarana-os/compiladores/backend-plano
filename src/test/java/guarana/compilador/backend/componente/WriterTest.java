package guarana.compilador.backend.componente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

class WriterTest {

    @Test
    @DisplayName("deve retornar codigo de maquina para notificação")
    void deveRetornarCodigoMaquinaParaNotificacao() {

        final Writer writer = Writer.mock(List.of("A").iterator());

        Assertions.assertEquals
                (
                        """
                                cmp 0x1, [esp + 1]
                                jne A
                                mov cx , v11_idade
                                add cx , v11_nome
                                call _write
                                A:\t""",
                        writer.generationCode()
                );

    }

    @Test
    @DisplayName("deve retornar codigo de maquina para notificação global")
    void deveRetornarCodigoMaquinaParaNotificacaoGlobal() {

        final Writer writer = Writer.mock(3, List.of("A").iterator(), Predicate.FALHA_GLOBAL);

        Assertions.assertEquals
                (
                        """
                                mov ax, [esp + 1]
                                and ax, [esp + 2]
                                and ax, [esp + 3]
                                cmp 0x0, ax
                                jne A
                                mov cx , v33_idade
                                add cx , v33_nome
                                call _write
                                A:\t""",
                        writer.generationCode()
                );

    }

}