package guarana.compilador.backend.componente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AttributeTest {

    @Test
    @DisplayName("Deve retornar o endereco da variavel")
    void deveRetornarEnderecoDaVariavel() {
        Assertions.assertEquals("v11_nome", Attribute.mock().addressUnit());
    }

    @Test
    @DisplayName("Deve retornar o endereco da variavel")
    void deveRetornarUnidadeDaSecao() {
        Assertions.assertEquals("v11_nome db \"nome\"", Attribute.mock().sectionUnit());
    }

}