package guarana.compilador.backend.componente;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;

class ComponentTest {

    @Test
    @DisplayName("Deve traduzir plano de testes em código de maquina")
    void deveTraduzirPlanoDeTestesEmCodigoDeMaquina() {

        final Iterator<String> registers = List.of("A", "B", "C").iterator();

        final Component component = new Component
                (
                        new Plan(1, registers, "X", "asdjdsaidsajidsa", "disk_plan"),
                        List.of(Executor.mock(registers)),
                        List.of(Writer.mock(1, registers, Predicate.SUCESSO_GLOBAL)),
                        Driver.mock(1).dataIterable()
                );

        System.out.println(component.generationCode());

    }
}