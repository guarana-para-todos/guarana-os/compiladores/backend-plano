package guarana.compilador.backend.componente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class PredicateTest {

    @ParameterizedTest
    @ValueSource(strings = {"success", "failed", "success_global", "failed_global"})
    @DisplayName("deve retornar predicado")
    void deveRetornarPredicado(final String value) {
        Assertions.assertEquals(value, Predicate.from(value).value());
    }

    @Test
    @DisplayName("quando sucesso deve traduzir Para código de maquina")
    void quandoSucessoDeveTraduzirParaCodigoDeMaquina() {
        final String espero = """
                cmp 0x1, [esp + 1]
                jne A
                mov bx, 0x2
                A:\t""";
        Assertions.assertEquals(espero, Predicate.SUCESSO.generationCode("A", 1, () -> "mov bx, 0x2"));
    }

    @Test
    @DisplayName("quando falha deve traduzir Para código de maquina")
    void quandoFalhaDeveTraduzirParaCodigoDeMaquina() {
        final String espero = """
                cmp 0x0, [esp + 2]
                jne B
                mov bx, 0x4
                B:\t""";
        Assertions.assertEquals(espero, Predicate.FALHA.generationCode("B", 2, () -> "mov bx, 0x4"));
    }

    @Test
    @DisplayName("quando sucesso global com ticket = 1, deve traduzir Para código de maquina")
    void quandoSucessoGlobalTicketIgual1DeveTraduzirParaCodigoDeMaquina() {
        final String espero = """
                mov ax, [esp + 1]
                cmp 0x1, ax
                jne A
                mov bx, 0x2
                A:\t""";
        Assertions.assertEquals(espero, Predicate.SUCESSO_GLOBAL.generationCode("A", 1, () -> "mov bx, 0x2"));
    }

    @Test
    @DisplayName("quando sucesso global com ticket = 2, deve traduzir Para código de maquina")
    void quandoSucessoGlobalTicketIgual2DeveTraduzirParaCodigoDeMaquina() {
        final String espero = """
                mov ax, [esp + 1]
                and ax, [esp + 2]
                cmp 0x1, ax
                jne A
                mov bx, 0x2
                A:\t""";
        Assertions.assertEquals(espero, Predicate.SUCESSO_GLOBAL.generationCode("A", 2, () -> "mov bx, 0x2"));
    }


    @Test
    @DisplayName("quando falha global com ticket = 1, deve traduzir Para código de maquina")
    void quandoFalhaGlobalTicketIgual1DeveTraduzirParaCodigoDeMaquina() {
        final String espero = """
                mov ax, [esp + 1]
                cmp 0x0, ax
                jne A
                mov bx, 0x2
                A:\t""";
        Assertions.assertEquals(espero, Predicate.FALHA_GLOBAL.generationCode("A", 1, () -> "mov bx, 0x2"));
    }

    @Test
    @DisplayName("quando falha global com ticket = 2, deve traduzir Para código de maquina")
    void quandoFalhaGlobalTicketIgual2DeveTraduzirParaCodigoDeMaquina() {
        final String espero = """
                mov ax, [esp + 1]
                and ax, [esp + 2]
                cmp 0x0, ax
                jne A
                mov bx, 0x2
                A:\t""";
        Assertions.assertEquals(espero, Predicate.FALHA_GLOBAL.generationCode("A", 2, () -> "mov bx, 0x2"));
    }

}