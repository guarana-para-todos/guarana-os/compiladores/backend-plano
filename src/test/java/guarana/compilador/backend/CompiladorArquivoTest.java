package guarana.compilador.backend;

import guarana.compilador.backend.componente.Translate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

class CompiladorArquivoTest {

    @ParameterizedTest
    @CsvSource({
            "entrada/parse_pass/plan_2_testes_com_n_global.plan,saida/plan_2_testes_com_n_global.s",
            "entrada/parse_pass/plan_teste_sem_notificacao.plan,saida/plan_teste_sem_notificacao.s"
    })
    @DisplayName("deve traduzir plan para código de maquina")
    void deveTraduzirPlanParaCodigoDeMaquina(final String entrada, final String saida) throws IOException {

        final String path = Objects.requireNonNull(getClass().getClassLoader().getResource(saida)).getPath();

        try (final BufferedReader reader = new BufferedReader(new FileReader(path))) {

            final String file = reader.lines().collect(Collectors.joining("\n")).trim();

            final Translate translate = new CompiladorArquivo(entrada);

            final String code = translate.generationCode().trim();

            Assertions.assertEquals(file, code);

        }

    }

    @ParameterizedTest
    @ValueSource(strings = {
            "entrada/parse_error/vazio.plan",
            "entrada/parse_error/componente-vazio.plan",
            "entrada/parse_error/driver-notificacao-vazia.plan",
            "entrada/parse_error/driver-teste-vazio.plan",
            "entrada/parse_error/notificacao-vazia",
            "entrada/parse_error/teste-vazio.plan",
            "entrada/parse_error/driver-atributo-sem-valor.plan"
    })
    @DisplayName("quando cadeia de caracteres não reconhecida pelo parse deve subir exceção")
    void quandoCadeiaCaracteresNaoReconhecidaPeloParseDeveSubirException(final String nomeArquivo) {

        final Translate translate = new CompiladorArquivo(nomeArquivo);

        Assertions.assertThrows(CompileErrorException.class, translate::generationCode)
                .printStackTrace();

    }

    static final class CompiladorArquivo implements Translate {

        CompiladorArquivo(final String filePath) {
            this.filePath =
                    Objects.requireNonNull
                                    (
                                            getClass().getClassLoader().getResource(filePath)
                                    )
                            .getPath();
        }

        private final String filePath;

        @Override
        public String generationCode() {

            try (final BufferedReader reader = new BufferedReader(new FileReader(filePath))) {

                final String file = reader.lines().collect(Collectors.joining());

                final AtomicReference<Translate> translate = new AtomicReference<>();

                new Compilador(translate::set).traduzir(file);

                return Objects.requireNonNull(translate.get()).generationCode();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }

    }
}
